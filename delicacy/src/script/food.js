// (function (window) {
//     function Animation(node, moveLength, startLength, moveTime, animationType) {
//         return new Animation.prototype.init(node, moveLength, startLength, moveTime, animationType);
//     }
//
//     Animation.prototype = {
//         constructor: Animation,
//         init: function (node, moveLength, startLength, moveTime, animationType) {
//             this.node = node;
//             this.moveLength = moveLength;
//             this.startLength = startLength;
//             this.animationType = animationType;
//             this.stmp = 17 / moveTime;
//             this.time = 0;
//             this.move();
//         },
//         tween: {
//             linear: [0, 0, 1, 1]
//         },
//         bezier: function (point, t) {
//             var controlPoint = [[0, 0], [point[0], point[1]], [point[2], point[3]], [1, 1]];
//             var tmp = 1 - t;
//             var coefficient = [1, 3, 3, 1];
//             var len = coefficient.length;
//             return coefficient.reduce(function (prev,value,key) {
//                 return prev + value * Math.pow(tmp,len - key -1) * Math.pow(t,key) * controlPoint[key][1];
//             }, 0) * (this.moveLength - this.startLength);
//         },
//         move: function () {
//             var that = this;
//             var timer = setInterval(function () {
//                 var x = that.bezier(that.tween[that.animationType], that.time);
//                 that.node.style.left = x + that.startLength + 'px';
//                 if (that.time > 1) {
//                     clearInterval(timer);
//                     that.node.style.left = that.moveLength + 'px';
//                 }
//                 that.time += that.stmp;
//             }, 17)
//         }
//     };
//     Animation.prototype.init.prototype = Animation.prototype;
//     Animation.prototype.init.prototype.constructor = Animation.prototype.init;
//     window['Animation'] = Animation;
// }(window));

// window.onload = function () {
//     //获取图片容器
//     var scroll = document.getElementsByClassName('bg_img')[0];
//     //获取到所有的焦点按钮
//     var buttons = document.getElementsByClassName('dot')[0].getElementsByTagName('span');
//     //定时器
//     var timer = null;
//     //当前显示图片的索引
//     var index = 0;
//     //存储点亮的小圆点
//     var arr = [buttons[0]];
//
//     //运动
//     function autoMove(){
//         timer = setInterval(function(){
//             Animation(scroll,-720 + index * -720,index * -720,1000,'linear');
//             index === 3 ? index = 0 : index++;
//             showButton();
//         },2000);
//     }
//     autoMove();
//
//
//     //点亮小圆点
//     function showButton(){
//         arr[0].className = '';
//         buttons[index].className = 'on';
//         arr[0] = buttons[index];
//     }
//
//     //小圆点击
//     function btnlick(){
//         for(var i = 0; i < buttons.length; i++){
//             buttons[i].onclick = function(){
//                 var dataIndex = this.getAttribute('data-Index');
//                 clearInterval(timer);
//                 Animation(scroll,dataIndex * -720,index * -720,1000,'linear');
//                 index = parseInt(dataIndex);
//                 autoMove();
//                 showButton();
//             }
//         }
//     }
//     btnlick();
//
//     //放上去暂停
//     function showTime(){
//         scroll.addEventListener('mouseover',function(e){
//
//             if(e.target.nodeName === 'IMG'){
//                 clearInterval(timer);
//             }
//         },false);
//         scroll.addEventListener('mouseout',function(e){
//             if(e.target.nodeName === 'IMG'){
//                 autoMove();
//             }
//         },false);
//     }
//     showTime();
// };



window.onload=function(){
    var heart=document.getElementsByTagName('i')[0];
    var num=document.getElementsByClassName('text_color')[0];

    heart.onclick=function(){
        this.className=
        this.style.background='red';
        console.log(this);
    }

};
