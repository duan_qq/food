﻿//de包的引入方式 相当于网页中的 script src
//加载插件
var gulp = require("gulp");
//gulp-load-plugins 只能加载gulp开头插件，并遵循驼峰式写法
var $ = require("gulp-load-plugins")();
// $.sequence = require('run-sequence').use(gulp);
$.browserSync = require('browser-sync').create();
//注明执行环境(开发环境DEV和生产环境PROD)

var rem = require("postcss-px2rem");
var DEV = true;
// var DEV=false;
//注明资源入口和出口
var input = __dirname + "/src";
var output = __dirname + "/dist";

var opt = {
    html:"/",
    preStyle:"/sass",
    style:"/style",
    script:"/script",
    image:"/image",
    icon:"/image/icon",
    sprite:"/img/sprite",
    rem:"/rem"
};

// console.log(typeof gulp);//gulp 是一个对象
// gulp 有五个方法 分别是
// src()    资源入口，将文本文件转化成二进制流
// pipe()   二进制流流动的管道
// dest()   资源出口  将二进制流转化为文本  并输出到指定文件夹
// watch(); 监视文件是否发生改动  如果改动 自动执行规定的自动化流程
// task(taskname[string],fn{returns})
//定义合并图标为雪碧图精灵图

gulp.task("serve",function () {
    $.browserSync.init({
        server:{
            baseDir:output
            // index:""
        }
    });
    gulp.watch(output+"/**/*.*").on("change", $.browserSync.reload);
});

gulp.task("spritesmith", function () {
    return gulp.src(input+opt.icon+"/**/*.png")
        .pipe($.spritesmith({
            // retinaSrcFilter:["./image/*@2x.png"],
            // retinaImgName:"sprite@2x.png",
            cssName:"sprite.css",
            imgName:"sprite.png",
            algorithm:"top-down",
            padding:0
        }))
        .pipe(gulp.dest(output+opt.icon))
});
//定义自动化操作之前清空目的文件夹
gulp.task("clean", function () {
    gulp.src([output+"/*"],{read:false})
        .pipe($.clean());
});

gulp.task("sass", function () {
    return gulp.src(input + opt.preStyle + "/**/*.scss")
        .pipe($.plumber())
        .pipe($.sass())
        .pipe($.plumber.stop())
        .pipe(gulp.dest(input+opt.rem));
});
//定义压缩css文件任务  gulp-minify-css
gulp.task("minifyCss", function () {
    return gulp.src(input + opt.style + '/**/*.css')
        .pipe($.autoprefixer())
        // .pipe($.px2remPlugin({
        //     width_design:750
        // }))
        .pipe($.if(!DEV, $.minifyCss()))
        .pipe($.if(!DEV, $.rev()))
        .pipe(gulp.dest(output+opt.style))
        .pipe($.if(!DEV, $.rev.manifest()))
        .pipe($.if(!DEV, gulp.dest(__dirname+'/rev/style')))
});

//==========================================
gulp.task('rem', function() {
    var px2rem = [rem({remUnit:37.5})];
    return gulp.src(input + opt.rem + '/**/*.css')
        .pipe($.postcss(px2rem))
        .pipe(gulp.dest(input+opt.style))
});

gulp.task("reload", function () {
    $.browserSync.reload()
});
//定义压缩图片任务
gulp.task("imagemin", function () {
    return gulp.src(input+opt.image+"/**/*.{jpg,jpeg,gif,png,webp,apng}")
        .pipe($.if(!DEV, $.imagemin(
            {
                optimizationLevel: 0, //类型：Number  默认：3  取值范围：0-7（优化等级）
                progressive: true, //类型：Boolean 默认：false 无损压缩jpg图片
                interlaced: true //类型：Boolean 默认：false 隔行扫描gif进行渲染
            }
        )))
        // .pipe(webp())
        // .pipe(_.rev())
        .pipe(gulp.dest(output+opt.image))
    // .pipe(_.rev.manifest())
    // .pipe(gulp.dest("./dest/rev/img"));
});
//html 压缩
gulp.task("minifyHtml",function () {
    return gulp.src(input+opt.html+"/**/*.html")
        .pipe($.if(!DEV, $.minifyHtml()))
        .pipe($.if(!DEV,$.rev()))
        .pipe(gulp.dest(output))
        .pipe($.if(!DEV,$.rev.manifest()))
        .pipe($.if(!DEV,gulp.dest(__dirname+'/rev/html')))
});
//js压缩
gulp.task("uglify",function () {
    return gulp.src(input+'/**/*.js')
        .pipe($.uglify())
        .pipe($.if(!DEV,$.rev()))
        .pipe(gulp.dest(output))
        .pipe($.if(!DEV,$.rev.manifest()))
        .pipe($.if(!DEV,gulp.dest(__dirname+"/rev/script")))
});

gulp.task('rev',function () {
    return gulp.src([__dirname+"/rev/**/*.json",input+"/**/*.{html,css}"])
        .pipe($.revCollector({
            replaceReved:true,
            dirReplacements:{
                // "../css/":"../css/",
                // "./image/":"/",
                // "./script/":"/"
            }
        }))
        .pipe($.if(!DEV, $.minifyHtml()))
        .pipe(gulp.dest(output))
});

// gulp.src('./src/*.scss')
//     .pipe(plumber())
//     .pipe(sass())
//     .pipe(uglify())
//     .pipe(plumber.stop())
//     .pipe(gulp.dest('./dist'));

gulp.task("watch",function () {
    gulp.watch(input+"/**/*.html",['minifyHtml']);
    gulp.watch(input+opt.style+"/**/*.css",['minifyCss']);
    gulp.watch(input+opt.script+"/**/*.js",['uglify']);
    gulp.watch(input+opt.preStyle+"/**/*.scss",["sass"])
});
gulp.task("default",$.sequence("clean","sass","rem","imagemin","minifyHtml","uglify", "minifyCss","spritesmith","watch","serve"));
gulp.task("prod", $.sequence("clean","sass","rem","imagemin","uglify","minifyCss","spritesmith","rev"));